<!-- Write small summary as the title -->

## Type of Mistake
<!--  What is the type of the issue. -->
- [ ] Major spelling mistake 
- [ ] Minor spelling mistake
- [ ] Duplicate translation
- [ ] Wrong translation

## Surah and Ayah No
<!-- Mention the Surah & Ayah no. -->
1.
2.

## Extras
<!--  Provide relevant screenshots or related information to this (optional) -->

/label ~"type::bug" ~"triage::none" ~"content"
